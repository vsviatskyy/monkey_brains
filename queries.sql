--Select all direct employees of Employee with ID=2
select emp_name from Employee where boss = 2;
--Select all direct employees of Employee with rank="VPO"
select B.emp_name from Employee as A, Employee as B where A.rank="VPO" and A.empID=B.boss;
--Query (1)
SELECT * FROM  where thread='' ORDER BY date DESC LIMIT 10;
--Query (2)
SELECT * FROM /*table*/ ORDER BY date DESC LIMIT 10;
