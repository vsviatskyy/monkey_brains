-- Insert
insert into Company values(0,"RVAJ Corp.",0,0);

insert into Status values('None',0 );
insert into Status values('Silver',20 );
insert into Status values('Gold',50);
insert into Status values('Platinum',70);

insert into RegUser values(0,"Victor Sviatskyy",7776665544,"sviatskyy@RVAJ.com",
        "sviatskyy7","victor7",1111222233334444,0,0,"None");
insert into RegUser values(1,"Andres Aceves",7776665544,"aceves@RVAJ.com",
        "aceves7","andres7",1111222233334444,0,0,"None");
insert into RegUser values(2,"Roque Fernandes",7776665544,"fernandes@RVAJ.com",
        "fernandes7","roque7",1111222233334444,0,0,"None");
insert into RegUser values(3,"JP",7776665544,"JP@RVAJ.com",
        "JP7","JP7",1111222233334444,0,0,"None");


insert into Employee values(0,"Nina Hartley","22 Acacia Ave",5557774433,"hartley@RVAJ.com",
        000000000,null,"CEO");
insert into Employee values(1,"BiBi Jones","22 Acacia Ave",5557774433,"jones@RVAJ.com",
        000000001,0,"VPO");
insert into Employee values(2,"Gianna Michaels","22 Acacia Ave",5557774433,"michaels@RVAJ.com",
        000000002,0,"VPM");
insert into Employee values(3,"Eva Angelina","22 Acacia Ave",5557774433,"angelina@RVAJ.com",
        000000003,1,"DoAtt");
insert into Employee values(4,"Asa Akira","22 Acacia Ave",5557774433,"akira@RVAJ.com",
        000000004,1,"DoAtt");
insert into Employee values(5,"Aria Giovanni","22 Acacia Ave",5557774433,"giovanni@RVAJ.com",
        000000005,2,"CEoW");
insert into Employee values(6,"Raven Riley","22 Acacia Ave",5557774433,"riley@RVAJ.com",
        000000006,2,"FO");
insert into Employee values(7,"Sasha Grey","22 Acacia Ave",5557774433,"grey@RVAJ.com",
        000000007,2,"FO");
insert into Employee values(8,"Sarah Jay","22 Acacia Ave",5557774433,"jay@RVAJ.com",
        000000008,5,"AE");
insert into Employee values(9,"Bree Olsen","22 Acacia Ave",5557774433,"olsen@RVAJ.com",
        000000009,5,"AE");
insert into Employee values(10,"Carmella Bing","22 Acacia Ave",5557774433,"bing@RVAJ.com",
        000000010,3,"STP");
insert into Employee values(11,"Cody Lane","22 Acacia Ave",5557774433,"lane@RVAJ.com",
        000000011,4,"STP");
insert into Employee values(12,"Desirae Spencer","22 Acacia Ave",5557774433,"spencer@RVAJ.com",
        000000012,4,"STP");
insert into Employee values(13,"Ashlynn Brooke","22 Acacia Ave",5557774433,"brooke@RVAJ.com",
        000000013,4,"STP");

insert into Attraction values(0,"Pink Monkey","Exotic Dancers","750 South Clinton Avenue, Chicago, IL",
        "19:00PM","2:00AM","Chicago",0,0,0,3,5);
insert into Attraction values(1,"Skybox","We serve alcoholic beverages and hide nothing",
        "16700 S. Halsted St, Harvey, IL", "20:30PM","3:00AM","Chicago",1,0,0,0,1);
insert into Attraction values(2,"Heavenly Bodies","Adult Entertainment ",
        "2835 E Higgins Rd, Elk Grove Village, IL", "21:30PM","2:00AM","Chicago",0,0,0,4,3);

insert into Doatt_stuff values(3,0,0);
insert into Doatt_stuff values(4,2,0);


insert into Tickets values(0,20.00);
insert into Tickets values(1,25.00);
insert into Tickets values(2,15.00);
insert into Tickets values(3,40.00);
insert into Tickets values(4,55.00);

insert into Opens values(0,0);
insert into Opens values(1,1);
insert into Opens values(2,2);
insert into Opens values(3,0);
insert into Opens values(3,2);
insert into Opens values(4,0);
insert into Opens values(4,1);
insert into Opens values(4,2);

