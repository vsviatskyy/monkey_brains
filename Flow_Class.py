# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
from __future__ import print_function
import shlex
from User_Class import *
from Forum_Class import *
import sys
user=User_Class()
forum=Forum_Class()
class Flow_Class:
        
    def loop(self,con,cursor):
        
        # Initial username to be displayed
        usr_name = "guest"
        # Currently logged in user
        authflag = None
        # Rank of currently logged in employee
        rank = None

        command = "help"

        print("\n")
        print("\n")
        print("\n")
        print("\n")
        print("\n")
        print("\n")
        print("\n")
        print("\n")
        print("\n")
        print("\n")
        print("\n")
        print("\n")

        print("8 888888888o.   `8.`888b           ,8'          .8.                     8 8888 ")      
        print("8 8888    `88.   `8.`888b         ,8'          .888.                    8 8888 ")  
        print("8 8888     `88    `8.`888b       ,8'          :88888.                   8 8888 ")  
        print("8 8888     ,88     `8.`888b     ,8'          . `88888.                  8 8888 ")  
        print("8 8888.   ,88'      `8.`888b   ,8'          .8. `88888.                 8 8888 ")  
        print("8 888888888P'        `8.`888b ,8'          .8`8. `88888.                8 8888 ")  
        print("8 8888`8b             `8.`888b8'          .8' `8. `88888.    88.        8 8888 ")  
        print("8 8888 `8b.            `8.`888'          .8'   `8. `88888.   `88.       8 888' ")  
        print("8 8888   `8b.           `8.`8'          .888888888. `88888.    `88o.    8 88'  ")  
        print("8 8888     `88.          `8.`          .8'       `8. `88888.     `Y888888 '    ")  
        print("")
        print("")
        print("")
                                                                    
        print("   ,o888888o.        ,o888888o.      8 888888888o.   8 888888888o   ")
        print("   8888     `88.   . 8888     `88.   8 8888    `88.  8 8888    `88. ")
        print(",8 8888       `8. ,8 8888       `8b  8 8888     `88  8 8888     `88 ")
        print("88 8888           88 8888        `8b 8 8888     ,88  8 8888     ,88 ")
        print("88 8888           88 8888         88 8 8888.   ,88'  8 8888.   ,88' ")
        print("88 8888           88 8888         88 8 888888888P'   8 888888888P'  ")
        print("88 8888           88 8888        ,8P 8 8888`8b       8 8888         ")
        print("`8 8888       .8' `8 8888       ,8P  8 8888 `8b.     8 8888         ")
        print("   8888     ,88'   ` 8888     ,88'   8 8888   `8b.   8 8888         ")
        print("    `8888888P'        `8888888P'     8 8888     `88. 8 8888         ")
        print("")
        print("")
        print("")


        while(command!='exit'):
            
            # Some of this stuff should probably go into login function
            if(command=='login'):
                if(authflag==None):
                    inu=raw_input("Please enter your Username: ")
                    inp=raw_input("Please enter your Password: ")
                    inputs=[inu,inp]
                    if(len(inputs)==2):
                        
                        authflag = user.login(con,cursor,inputs[0],inputs[1])
                        
                        if(authflag!=None):
                            rank = authflag[1]
                            authflag = authflag[0]
                            usr_name = inputs[0]
                        
                            if(rank!=False):
                                self.employee_loop(con,cursor,authflag,
                                    inputs[0],rank)
                                usr_name = "guest"
                                authflag=None
                                rank = None
                                                    
                    else:
                        print("Insufficient information\n")
                else:
                    print("Already logged in as {}".format(usr_name))
                    print("Log out first before you can login again")
            
            elif(command=='logout'):
                if(authflag ==None):
                    print("Not logged in")
                else:
                    usr_name = "guest"
                    authflag = None
                    print("Logging out")
            
            elif(command=='register'):
                if(authflag==None):
                    user.register(con,cursor)
                else:
                    print("Please logout first")
    
            elif(command=='view'):
                if(authflag!=None):
                    user.viewuser(cursor,authflag)
                else:
                    print("Please login first")
    
            elif(command=='update'):
                if(authflag!=None):
                    print("You can update phone,email,usr_name,usr_pswd,credcard")
                    change=raw_input("What do you want changed? ")
                    new=raw_input("Input new {}: ".format(change))
                    user.moduser(con,cursor,authflag,change,new)
                else:
                    print("Please login first")

            elif(command=='new thread'):
                if(authflag!=None):
                    thrinput=raw_input("Name your thread: ")
                    thrdid=forum.crthread(con,cursor,thrinput,authflag)
                    if(thrdid!=None):
                        print("Thread Created!")
                        commnt=raw_input("Type the first comment: ")
                        cmntid=forum.crcomment(con,cursor,thrinput,commnt,authflag)
                        if(cmntid!=None):
                            print("Comment added")
                        else:
                            print("Comment addition unsuccesful")
                    
                    else:
                        print("Choose different name.")
                else:       
                    print("Please login first")


            elif(command=='view threads'):
                    forum.viewthreads(con,cursor)


            elif(command=='comment'):
                if(authflag!=None):
                    trdnm=raw_input("What thread? ")
                    comet=raw_input("Whats your comment? ")
                    cmtid=forum.crcomment(con,cursor,trdnm,comet,authflag)
                    if(cmtid!=None):
                        print("Comment added")
                    else:
                        print("Comment addition unsuccesful")
                else:       
                    print("Please login first")

            elif(command=='view comments'):
                    trdnm=raw_input("For what thread? ")
                    choice=raw_input("Want all or recent comments? ")
                    forum.viewcomments(con,cursor,trdnm,choice)


            elif(command=='threads with comments'):
                
                    print("Showing all comments and their threads")
                    forum.viewthreadcoms(con,cursor)

            elif(command=='like'):
                forum.like(con,cursor)
                
            elif(command=='help'):
                print("Available commands:"
                    "\n--User Management:\n"
                    "register, login, logout, view, update\n"
                    "--Tickets:\n"
                    "viewtickets, buy \n" 
                    "--Forums:\n"
                    "_View:\n"
                    "threads with comments, view threads, view comments\n"
                    "_Post:\n"
                    "new thread, comment, like\n"
                    "--System:\n"
                    "help, exit")
            
            elif(command=='viewtickets'):
                user.viewtickets(cursor)
            
            elif(command=='buy'):
                if(authflag!=None):
                    user.buyticket(con,cursor,authflag)
                else:
                    print("Please login first")

            command=raw_input(" {}@RVAJ $ ".format(usr_name));

        print("Bye")    
    
        
    def employee_loop(self,con,cursor,authflag,usr_name,rank):
        
        command = "help"
        while(command!='logout'):
        
            if(command=='exit'):
                print("Bye")
                sys.exit(0)
            
            elif(command=='help'):
                print("Available commands:\n"
                "--User Management:\n"
                "view\n"
                "--Company management:\n"
                "delegate, set_staff_level\n"
                "--Reports:\n"
                "revenue, view_staff_level\n"
                "--System:\n"
                "logout, help, exit")
            elif(command=='view'):
                if(rank=='CEO' or rank=='VPM' or rank=='CEoW'):
                    ruID = raw_input("Provide ID of the user you "
                        "wish to view ");
                    user.viewuser(cursor,ruID)
                else:
                    print("You are not authorized to run "
                            "this command")

            elif(command=='delegate'):
                if(rank=='CEO'):
                    attID = raw_input("Which Attraction "
                        "would you like to delegate: ") 
                    self.delegate(con,cursor,attID)
                    
                else:
                    print("You are not authorized to run "
                        "this command")

            elif(command=='set_staff_level'):
                if(rank=='CEO'):
                    attID=raw_input("Which attraction: ")
                    level=raw_input("How many workers: ")
                    self.staff_level(con,cursor,attID,level)
                elif(rank=='DoAtt'):
                    attID = raw_input("Which Attraction "
                        "would you like to delegate: ")
                    if(self.isDoAtt_manage(con,cursor,authflag,attID)==(True,True)):
                        level=raw_input("How many workers: ")
                        self.staff_level(con,cursor,attID,level)
                else:
                    print("You are not authorized to run "
                        "this command")
            elif(command=='revenue'):
                if(rank=='CEO'):
                    user.revenue(cursor)
                else:
                    print("You are not authorized to run "
                        "this command")
            elif(command=='view_staff_level'):
                if(rank=='CEO' or rank=='VPO' or rank=='DoAtt'):
                    user.viewstaff(cursor)
                else:
                    print("You are not authorized to run "
                        "this command")


            command=raw_input(" {} $ ".format(usr_name));

    # Delegate staffing level for a given attID
    def delegate(self,con,cursor,attID):
        cursor.execute("select compID from Attraction where "
                "attID={};".format(attID))
        compID = cursor.fetchone()[0]     

        if(compID!=0):
            print("We don't manage this attraction. It belongs to "
                "company {}".format(compID))
            return

        cursor.execute("update Doatt_stuff set perm=1 where "
                "aID={}".format(attID))
        con.commit()

    def staff_level(self,con,cursor,attID,level):
        # Check if we manage this attraction
        cursor.execute("select compID from Attraction where "
                "attID={};".format(attID))
        compID = cursor.fetchone()

        if(compID==None):
            print("Attraction don't exist")
            return
        else:
            compID = compID[0]

        if(compID!=0):
            print("We don't manage this attraction. It belongs to "
                "company {}".format(compID))
            return

        # Update staffing level
        cursor.execute("update Attraction set staff_level={} where attID"
            "= {}".format(level,attID))
        con.commit()

    # Returns a boolean tuple (manage,staffing)
    # where manage=True when DoAtt manages given attraction
    # and staffing=True when DoAtt has been delegated staffing level by CEO
    def isDoAtt_manage(self,con,cursor,DoAttID,AttID):
        manage=False
        staffing=False
        cursor.execute("select AttID from Attraction where DoAttID={} "
                "and AttID={};".format(DoAttID,AttID))
        if(cursor.fetchone()!=None):
            manage=True
        cursor.execute("select perm from Doatt_stuff where "
                "aID={};".format(AttID))
        x=cursor.fetchone()
        if(x!=None): 
            if(x[0]==1):
                staffing=True
            else:
                print("Ask CEO to delegate staffing "
                    "level for this Attraction")
        else:
            print("You don't manage this attraction")


        return (manage,staffing)
