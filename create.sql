-- CREATE TABLE THING
/* Maybe make trigger for one time bonus */
create table Company(
       compID           int not null,         -- Company ID
       compName         varchar(20), 
       rev              numeric(19,2),        -- Revenue
       ext_rev          numeric(19,2),        -- External Revenue
       primary key(compID));


create table Status(
        status varchar(10) not null, 
        discount int not null,                   -- discount percentage
        primary key(status));


create table RegUser(
       ruID            int not null,
       ru_name         varchar(30) not null,
       phone           numeric(10,0) not null,
       email           varchar(30) not null,
       usr_name        varchar(20) not null,
       usr_pswd        varchar(20) not null,
       credcard        numeric(16,0) not null,
       rewardpnts      int not null,
       totalpnts       int not null,  
       status          varchar(10) not null,
       primary key(ruID),
       unique(email),
       unique(usr_name),
       foreign key(status) references Status(status));

create table Reward(
        ruID   int not null,
        Statusnew    varchar(10) not null,
        foreign key(ruID)     references RegUser(ruID),
        foreign key(Statusnew) references Status(status));

 
create table Employee(
      empID            int not null,
      emp_name         varchar(15) not null,
      address          varchar(20) not null,
      phone            numeric(10,0) not null,
      email            varchar(30) not null,
      ssn              numeric(9,0) not null,
      boss             int,
      rank             varchar(20) not null,
      primary key(empID),
      foreign key(boss) references Employee(empID));

/* Attractions that belong to other companies will have a non-zero
 * compID(compID=0 is our company) and the doattID for such attractions
 * will be set to zero.
 */
create table Attraction(
      attID            int not null,
      att_name         varchar(15) not null,  -- Attraction Id
      att_desc         varchar(60) not null,  -- Attraction Name
      address          varchar(60) not null,  -- Attraction Description
      hropen           varchar(7)  not null,  -- Hours Open
      hrclose          varchar(7)  not null,  -- Hours Close
      city             varchar(10) not null,  -- City where Attraction
      compID           int not null,          -- Company Id
      like_cnt         int not null,          -- Like Count
      dislike_cnt      int not null,          -- Dislike Count
      doattID          int not null,          -- Director of Attraction ID
      staff_level      int not null,          -- Staffing level(# of workers needed)
      primary key(attID),
      unique(att_name,city),                  -- Director OF Attraction
      foreign key(doattID) references Employee(empID));            


/* perm Variable is set to 0 for not permitted 
   and 1 for permitted by CEO*/
create table Doatt_stuff(
        dID             int not null,
        aID             int not null,
        perm            int not null,
        primary key(aID),
        foreign key(dID) references Employee(empID),
        foreign key(aID) references Attraction(attID));

create table Tickets( 
        tickID int not null, 
        cost numeric(19,2) not null, 
        primary key(tickID));

create table Purchase( 
        purID int not null, 
        ruID int not null, 
        tickID int not null, 
        primary key(purID),
        foreign key(tickID) references Tickets(tickID), 
        foreign key(ruID) references RegUser(ruID));

create table Opens(
        tickID int not null, 
        attID int not null, 
        primary key(tickID, attID),
        foreign key(tickID) references Tickets(tickID),
        foreign key(attID)  references Attraction(attID));


create table Threadz(
       thid int not null,
       thd_name varchar(20) not null,
       unique(thd_name),
       primary key(thid));

create table Comments(
       comid int not null,
       content varchar(160),
       likes int not null,
       thread_id int not null,
       primary key(comid),
       foreign key(thread_id) references Threadz(thid));
/*
 create table JobRespons(
      empID            int not null,
      resp_descript    varchar(25) not null,
      attID            int not null,
      foreign key(empID) references Employee(empID),
      foreign key(attID) references Attraction(attID));
*/
