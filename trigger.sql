delimiter $$
create trigger rewardmsg before update on RVAJ.RegUser
FOR EACH ROW
BEGIN
IF (new.totalpnts >= 400) and (old.status = 'None') THEN
        set new.status = 'Silver';
        insert into Reward(ruID,Statusnew) values(old.ruID,new.status);
ELSEIF (new.totalpnts >= 800) and (old.status = 'Silver') THEN
        set new.status = 'Gold';
        insert into Reward(ruID,Statusnew) values(old.ruID,new.status);
ELSEIF (new.totalpnts >= 1500) and (old.status = 'Gold') THEN
        set new.status = 'Platinum';
        insert into Reward(ruID,Statusnew) values(old.ruID,new.status);

END IF;
END$$
delimiter ;
