# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
class User_Class:
    
    # Adds user to out database
    def register_user(self,con,cursor,ru_name,phone,email,usr_name,
            usr_pswd,credcard,rewardpts,status):
        
        cursor.execute("select max(ruID)+1 from RegUser;")
        nextID = cursor.fetchone()[0]
    
        if(nextID==None):
            nextID=0
         
        cursor.execute("insert into RegUser values \
            ({},'{}',{},'{}','{}','{}',{},{},{},'{}');".format(nextID,ru_name,
                phone,email,usr_name,usr_pswd,credcard,rewardpts,rewardpts,status))
        con.commit()
        print("Registration Success!")
        return nextID


          
    # Welcomes authenticated user
    def login(self,con,cursor, usr_name, usr_pswd):
        authflag=self.authenticate(cursor,usr_name,usr_pswd)
        if(authflag!=None):
            print(" | |           ")
            print(" | |    $$$$$    -----------------------  ")
            print(" | |   $(^.^)$    Welcome to RVAJ {}!  ".format(usr_name))
            print(" | |   $$) ($$   -----------------------  ")
            print(" | |  /( ) ( )\    ")
            print(" | |_/ $) . ($ |   ")
            print(" | |   (     )/    ")
            print(" | |    \/  /      ")
            print(" | |    /  /       ")

            
            return (authflag,False) 
        else:
            authflag=self.authenticate_employee(cursor,usr_name,usr_pswd)
            if(authflag !=None):
                rank = authflag[1]
                authflag = authflag[0]
    
            if(authflag!=None):
                print("Back to work, slacker!")
                return (authflag,rank)
            else:
                print("Wrong username or password")
                print("Try again or register")
               
    # Registers a user
    def register(self,con,cursor):
        # Array to print information field for registration of user       
        meminfo = ["First Name","Last Name: ","Phone number: ","Email: ",
                "Username: ", "Password: ","Credit Card Number:"]
        # Array that contains information of registering user in order 
        # to be bassed to function register_user
        meminput=[]
        
        print("Complete following information\n")
        for i in range(0,len(meminfo)):
            print(meminfo[i])
            meminput.append(raw_input())
        ru_name="{} {}".format(meminput[0],meminput[1])
        regflag = self.register_user(con,cursor,ru_name,
            meminput[2],meminput[3],meminput[4],
            meminput[5],meminput[6],0,'None')
        return regflag 
        
    


    # Returns ruID if user is registered and None if not
    def authenticate(self,cursor,usr_name,usr_pswd):
        cursor.execute("select ruID from RegUser where usr_name='{}' and \
                usr_pswd='{}'".format(usr_name,usr_pswd))
        guy = cursor.fetchone()
        
        if(guy != None):
            return(guy[0])
        else:
            return None
    
    
    # Show user information
    def viewuser(self,cursor,ruID):
        hat = ["Name:\t\t","Phone number:\t","Email:\t\t",
            "Username:\t", "Password:\t","Credit Card:\t",
            "Reward Points:\t","Status Points\t","Status:\t\t"]
        
        cursor.execute("select * from RegUser where ruID={}".format(ruID))
        flagQ=cursor.fetchone()
        if(flagQ!=None):

            for i in range(1,len(flagQ)):
                print("{}{}".format(hat[i-1],flagQ[i]))
        else:
            print("No User in Database")
    
    # Takes ruID, param as [phone, email, usr_name, usr_pswd or credcard] and a new
    # value and changes the selected parameter to a new value
    def moduser(self,con,cursor,ruID,param,new_value):
        if(param in ["phone","email","usr_name","usr_pswd","credcard"] ):
            cursor.execute("UPDATE RegUser \
                SET {}='{}' WHERE ruID='{}'".format(param,new_value,ruID))
            con.commit()
    
    # Use user email for login and ssn for password
    # WARNING: when passed a non-integer for ssn, mysql throws a warning. 
    # Everything still works, though
    def authenticate_employee(self,cursor,email,ssn):
        cursor.execute("select empID,rank from Employee where email='{}' and "
            "ssn='{}'".format(email,ssn))
        
        guy = cursor.fetchone()
        
        if(guy != None):
            return(guy[0],guy[1])
        else:
            return None
    def viewtickets(self,cursor):
        cursor.execute("select count(tickID) from Tickets;")
        x = cursor.fetchone()[0]

        print("Ticket ID\tTicket Price\tAttractions")
        for i in range(0,x):
            self.viewticket(cursor,i,False)

    

    def viewticket(self,cursor,tickID,verbose):
        cursor.execute("select cost from Tickets where "
            "tickID={}".format(tickID))
        cost = cursor.fetchone()
        if(cost==None):
            print("Ticket does not exist")
            return
        else:
            cost = cost[0]

        cursor.execute("select att_name from Attraction natural join "
        "Opens where tickID={}".format(tickID))

        att_name = cursor.fetchall()
        
        if(verbose):
            print("Ticket ID\tTicket Price\tAttractions")

        tAtt = []

        for i in range(0,len(att_name)):
            tAtt.append(att_name[i][0])
        
        print("{}\t\t${}\t\t{}".format(tickID,cost,", ".join(tAtt)))

    def buyticket(self,con,cursor,ruID):
        tickID = raw_input("Which ticket? ")
        choice = None
        
        while(choice!='C' and choice!='R'):
            choice = raw_input("Would you like to pay with (C)redit card or "
             "(R)eward points?\n"
             "Type C or R: ")
            print(choice)

           
        cursor.execute("select count(attID) from Opens natural join "
        "Attraction where tickID={} and compID=0;".format(tickID))
        countour = float(cursor.fetchone()[0])
        
        cursor.execute("select count(attID) from Opens natural join "
        "Attraction where tickID={} and compID=1;".format(tickID))
        counttheir = float(cursor.fetchone()[0])
        
        if(countour+counttheir==0):
            print("No such ticket. Contact complaint department "
                "if you feel you have been robbed:\n"
                "https://upload.wikimedia.org/wikipedia/commons/a/a4/Complaint_Department_Grenade.jpg")
            return

        if(countour==0):
            frac = 0
        elif(counttheir==0):
            frac = 1
        else:
            frac = (countour)/(countour+counttheir)

        cursor.execute("select cost from Tickets where "
            "tickID={}".format(tickID))
        cost = float(cursor.fetchone()[0])
        
        if(choice=='C'):
            rewadj = 0
            totalpnts = 0.1*cost
        elif(choice=='R'):
            cursor.execute("select rewardpnts from RegUser where "
                    "ruId={};".format(ruID))
            rp = float(cursor.fetchone()[0])
            totalpnts = 0
        
            if(rp<cost):
                print("Not enough reward points. Try paying "
                        "with credit card")
                return
            else:
                rewadj = -cost*1.1    
        
        ext_rev = cost*(1-frac)
        revenue = cost*frac + 0.1*ext_rev
        
        rewpoints = 0.1*cost + rewadj

        cursor.execute("update Company set rev=rev+{0:.2f};".format(revenue))
        cursor.execute("update Company set ext_rev = "
            "ext_rev+{0:.2f};".format(ext_rev))
        cursor.execute("update RegUser set rewardpnts=rewardpnts+{} "
            "where ruID={}".format(rewpoints,ruID))
        cursor.execute("update RegUser set totalpnts=totalpnts+{} "
            "where ruID={}".format(totalpnts,ruID))
        
        cursor.execute("select max(purID)+1 from Purchase;")
        purID = cursor.fetchone()[0]
    
        if(purID==None):
            purID=0

        cursor.execute("insert into Purchase values({},{},{});".format(
            purID,ruID,tickID))
        con.commit()

        print("You just purchased ticket #{} for "
            "${}".format(tickID,cost))

        self.getReward(con,cursor,ruID)

    def revenue(self,cursor):
        cursor.execute("select rev from Company;")
        rev = cursor.fetchone()[0]
        print("The total revenue is: ${}".format(rev))

    def viewstaff(self,cursor):
        level = raw_input("Select Staffing level: ")

        cursor.execute("select att_name from Attraction where "
            "staff_level = {};".format(level))
        att_names = cursor.fetchall()

        if(att_names==None):
            print("No such attractions")
            return
        else:
            print("These Attractions have staffing level of "
                "{}".format(level))
            for i in range(0,len(att_names)):
                print(att_names[i][0])

    def getReward(self,con,cursor,ruID):
        cursor.execute("select Statusnew from Reward where ruID={};".format(ruID))
        status=cursor.fetchone()

        if(status==None):
            return
        else:
            status=status[0]
            print("\nCongratulations, your earned new membership status!\n"
                    "Your new status is {}".format(status))
            cursor.execute("delete from Reward where ruID={}".format(ruID))
            con.commit()


