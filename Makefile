# Makefile
PY = python2

files = create.sql drop.sql insert.sql trigger.sql queries.sql main.py Makefile README User_Class.py Flow_Class.py Forum_Class.py output_employee
pyfiles = main.py User_Class.py Flow_Class.py Forum_Class.py

run: $(pyfiles)
	$(PY) main.py 

check: $(pyfiles)
	$(PY) -m py_compile $(pyfiles)

commit: $(files)
	git add $(files)
	echo "Message: "; \
	read msg; \
	git commit -m "$$msg"

pull: $(files)
	git pull -u origin master

push: $(files)
	git push -u origin master

base: $(files)
	mysql -u bot --password=clonepenguin -D RVAJ
